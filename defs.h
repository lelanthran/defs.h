/*
 *           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                       Version 2, December 2004
 *
 *    Copyright (C) 2020 Henrique Dante de Almeida
 *
 *    Everyone is permitted to copy and distribute verbatim or modified
 *    copies of this license document, and changing it is allowed as long
 *    as the name is changed.
 *
 *               DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *      TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *     0. You just DO WHAT THE FUCK YOU WANT TO.
*/

#ifndef DEFS_H
#define DEFS_H

#if defined(__STDC__) || defined(__STDC_VERSION__)
#define HAVE_ISO_C 1
#endif

#if __STDC_VERSION__ >= 199409L
#define HAVE_ISO_C94 1
#endif

#if __STDC_VERSION__ >= 199901L
#define HAVE_ISO_C99 1
#endif

#if __STDC_VERSION__ >= 201112L
#define HAVE_ISO_C11 1
#endif

#if __STDC_VERSION__ >= 201710L
#define HAVE_ISO_C18 1
#endif

#if defined(__cplusplus)
#define HAVE_CXX 1
#endif

#if __cplusplus >= 199711L
#define HAVE_CXX98 1
#endif

#if __cplusplus >= 201103L
#define HAVE_CXX11 1
#endif

#if __cplusplus >= 201402L
#define HAVE_CXX14 1
#endif

#if __cplusplus >= 201703L
#define HAVE_CXX17 1
#endif

#if defined(unix) || defined(__unix) || defined(__unix__)
#define HAVE_UNIX 1
#endif

#if defined(_WIN16) || defined(_WIN32) || defined(_WIN64)
#define HAVE_WINDOWS 1
#endif

#if defined(__MACH__)
#define HAVE_MACH 1
#if defined(__APPLE__)
#define HAVE_APPLE 1
#endif
#endif

#if defined(__FreeBSD__) || defined(__NetBSD__) || defined(__OpenBSD__) \
	|| defined(__bsdi__) || defined(__DragonFly__) || defined(__APPLE__) \
	|| defined(BSD) || defined(_SYSTYPE_BSD)
#define HAVE_BSD 1
#endif

#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) \
	|| defined(__x86_64) || defined(_M_AMD64)
#define HAVE_AMD64 1
#endif

#if defined(__arm__) || defined(__thumb__) || defined(_ARM) \
	|| defined(_M_ARM) || defined(_M_ARMT) || defined(__arm)
#define HAVE_ARM 1
#endif

#if defined(__aarch64__)
#define HAVE_ARM64 1
#endif

#if defined(i386) || defined(__i386) || defined(__i386__) \
	|| defined(__i486__) || defined(__i586__) || defined(__i686__) \
	|| defined(__IA32__) || defined(_M_I86) || defined(_M_IX86) \
	|| defined(__X86__) || defined(_X86_) || defined(__THW_INTEL__) \
	|| defined(__I86__) || defined(__INTEL__) || defined(__386)
#define HAVE_IA32 1
#endif

#if defined(__mips__) || defined(mips) || defined(__mips) || defined(__MIPS__)
#define HAVE_MIPS 1
#endif

#if defined(__powerpc) || defined(__powerpc__) || defined(__powerpc64__) \
	|| defined(__POWERPC__) || defined(__ppc__) || defined(__ppc64__) \
	|| defined(__PPC__) || defined(__PPC64__) || defined(_ARCH_PPC) \
	|| defined(_ARCH_PPC64) || defined(_M_PPC) || defined(__ppc)
#define HAVE_POWERPC 1
#endif

#if defined(linux) || defined(__linux) || defined(__linux__)
#define HAVE_LINUX 1
#endif

#if defined(__gnu_linux__)
#define HAVE_GNU_LINUX 1
#endif

#if defined(__ANDROID__)
#define HAVE_ANDROID 1
#endif

#if defined(__CYGWIN__)
#define HAVE_CYGWIN 1
#endif

#if defined(__MSYS__)
#define HAVE_MSYS 1
#endif

#if defined(__GNUC__)
#define HAVE_GCC 1
#endif

#if defined(__llvm__)
#define HAVE_LLVM 1
#endif

#if defined(__clang__)
#define HAVE_CLANG 1
#endif

#if defined(_MSC_VER)
#define HAVE_MSC 1
#endif

#if defined(__MINGW32__) || defined(__MINGW64__)
#define HAVE_MINGW 1
#endif

#if HAVE_ISO_C
#include <stddef.h>
#include <limits.h>
#endif

#if HAVE_ISO_C99 || HAVE_CXX11
#include <stdbool.h>
#include <stdint.h>
#endif

#if HAVE_CXX
#include <cstddef>
#endif

#if HAVE_UNIX
#include <sys/types.h>
#include <unistd.h>
#endif

#if HAVE_WINDOWS
#include <windows.h>
#endif

#if defined(__GLIBC__) || defined(__GNU_LIBRARY__)
#define HAVE_GLIBC 1
#endif

#if defined(__BIONIC__)
#define HAVE_BIONIC 1
#endif

#if defined(__UCLIBC__)
#define HAVE_UCLIBC 1
#endif

#if defined(__GLIBCPP__) || defined(__GLIBCXX__)
#define HAVE_LIBSTDCXX 1
#endif

#if defined(_LIBCPP_VERSION)
#define HAVE_LIBCXX 1
#endif

#if defined(_POSIX_VERSION) || defined(_POSIX2_C_VERSION)
#define HAVE_POSIX 1
#endif

#if defined(_XOPEN_VERSION)
#define HAVE_XOPEN 1
#endif

#endif /* DEFS_H */
